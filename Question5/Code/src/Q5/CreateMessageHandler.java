package Q5;

import java.util.*;

/**
 * 
 */
public interface CreateMessageHandler {


    /**
     * @param handler
     */
    public void setNext(CreateMessageHandler handler);

    /**
     * @param message
     */
    public void handle(Message message);

}