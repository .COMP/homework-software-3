package Q5;

import java.util.*;

/**
 * 
 */
public class SendContext {

    /**
     * Default constructor
     */
    public SendContext() {
    }

    /**
     * 
     */
    private SendStrategy sendStrategy;




    /**
     * 
     */
    public void send(Message message) {
        sendStrategy.sendMessage(message);
    }

    /**
     * @param sendStrategy
     */
    public void setSendStrategy(SendStrategy sendStrategy) {
        this.sendStrategy=sendStrategy;
    }

}