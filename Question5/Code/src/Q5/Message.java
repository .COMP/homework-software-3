package Q5;

public class Message {

    private  String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Message(String type) {
        this.type = type;
    }
}
