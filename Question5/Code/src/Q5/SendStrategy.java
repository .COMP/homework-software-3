package Q5;

import java.util.*;

/**
 * 
 */
public interface SendStrategy {


    /**
     * @param message
     */
    public void sendMessage(Message message);

}