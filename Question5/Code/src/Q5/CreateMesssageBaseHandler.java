package Q5;

import java.util.*;

/**
 * 
 */
public class CreateMesssageBaseHandler implements CreateMessageHandler {

    /**
     * Default constructor
     */
    public CreateMesssageBaseHandler() {
    }

    /**
     * 
     */
    private CreateMessageHandler nextHandler;


    public CreateMessageHandler getNextHandler() {
        return nextHandler;
    }


    /**
     * @param handler
     */
    public void setNext(CreateMessageHandler handler) {
        this.nextHandler=handler;
    }

    /**
     * @param message
     */
    public void handle(Message message) {
        if(nextHandler!=null){
            nextHandler.handle(message);
        }
    }

}