package Q5;

import java.util.*;

/**
 * 
 */
public class SendMessage extends CreateMesssageBaseHandler {

    /**
     * Default constructor
     */
    public SendMessage() {
    }


    /**
     * @param message
     */
    public void handle(Message message) {
        System.out.println("Sending message");
        SendContext sendContext =new SendContext();
        SendStrategy sendStrategy=null;
        switch (message.getType()){
            case "In":
                sendStrategy=new SendInComing();
                break;
            case "Out":
                sendStrategy=new SendOutGoing();
                break;
            case "Note":
                sendStrategy=new SendNote();
                break;
        }
        if(sendStrategy!=null) {
            sendContext.setSendStrategy(sendStrategy);
            sendContext.send(message);
        }

    }



}