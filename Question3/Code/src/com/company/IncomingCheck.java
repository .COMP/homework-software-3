package Q3;

import java.util.*;

/**
 *
 */
public class IncomingCheck implements IncomingInterface {

    /**
     * Default constructor
     */
    public IncomingCheck() {
    }

    public IncomingCheck(Incoming incoming) {
        this.incoming = incoming;
    }

    /**
     *
     */
    private Incoming incoming;


    /**
     * @return
     */
    public boolean checkAccess() {
        // TODO implement here
        if(incoming.getType().equals("D"))
            return true;
        return false;
    }

    /**
     * @return
     */
    @Override
    public void processing() {
        // TODO implement here
        if (checkAccess())
            incoming.processing();
    }

}