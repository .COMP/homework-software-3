package Q3;

import java.util.*;

/**
 * 
 */
public class Incoming implements IncomingInterface {

    /**
     * Default constructor
     */
    public Incoming() {
    }

    public Incoming(String type) {
        this.type = type;
    }

    private String type;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return
     */
    public void processing() {
        // TODO implement here
        System.out.println("Incoming processing");
    }

}