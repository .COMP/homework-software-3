package com.company;

/**
 *
 */
public class AdapterCollegeSystem extends CollegeSystem {

    /**
     * Default constructor
     */
    public AdapterCollegeSystem() {
      this.service = new Service();
    }

    /**
     *
     */
    public Service service;



    /**
     * @param id
     * @param name
     */
    public void Request(String id, String name) {
      this.service.ServiceMethod(id, name, null);
    }

}
