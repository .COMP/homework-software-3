package com.company;

/**
 *
 */
public class Service {

    /**
     * Default constructor
     */
    public Service() {
    }


    /**
     * @param id
     * @param name
     * @param address
     */
    public void ServiceMethod(String id, String name, String address) {
        System.out.println("Your request information -> id:" + id + " -> name:"+name + " -> address:" + address);
    }

}
