package com.company;

public class Main {
  public static void main(String[] args) {

    // Define adapter object
    AdapterCollegeSystem adapterCollegeSystem = new AdapterCollegeSystem();

    // Call adapter method which use service(third party library)
    adapterCollegeSystem.Request("#123", "yamen abdalrahman");


  }
}
