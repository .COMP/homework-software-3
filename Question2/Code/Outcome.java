
import java.util.*;

/**
 * 
 */
public class Outcome extends Message {

    /**
     * Default constructor
     */
    public Outcome() {
    }


    /**
     * @return
     */
    public Outcome createOutcome(Note note) {
        System.out.println("Do some thing in create outcome message from note");
        return this;
    }

}