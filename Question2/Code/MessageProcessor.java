
import java.util.*;

/**
 * 
 */
public class MessageProcessor {

    /**
     * Default constructor
     */
    public MessageProcessor() {
    }

    /**
     * 
     */
    private Note note=new Note();

    /**
     * 
     */
    private Outcome outcome=new Outcome();





    /**
     * @param income
     */
    public Outcome process(Income income) {
        if(income!=null) {
            Note n = note.createNote(income);
            return outcome.createOutcome(n);
        }
        return null;
    }

}