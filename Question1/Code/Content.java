package com.company;

/**
 *
 */
public class Content extends Correspondence {

    /**
     * Default constructor
     */
    public Content(String text) {
      this.text = text;
    }

    /**
     *
     */
    private String text;

    /**
     *
     */
    public void print() {
      System.out.println("Your Content available is '" + this.text + "'");
    }

}
