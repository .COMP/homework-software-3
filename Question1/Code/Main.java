package com.company;

public class Main {
  public static void main(String[] args) {
    RelatedCorrespondence baseCorrespondence = new RelatedCorrespondence();

    // Define content
    Content content = new Content("Hello dear yammen!");

    // Define Related Correspondence
    RelatedCorrespondence relatedCorrespondence = new RelatedCorrespondence();
    relatedCorrespondence.add(new Content("Hello dear leen, im sub related correspondence one"));
    relatedCorrespondence.add(new Content("Hello dear lio, im sub related correspondence tow"));

    // Add item's to base correspondence
    baseCorrespondence.add(content);
    baseCorrespondence.add(relatedCorrespondence);

    // Print all content of correspondence's
    baseCorrespondence.print();

  }
}
