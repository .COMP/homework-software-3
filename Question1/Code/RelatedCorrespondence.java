package com.company;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class RelatedCorrespondence extends Correspondence {

  /**
   * Default constructor
   */
  public RelatedCorrespondence() {
    this.relatedCorrespondence = new ArrayList<>();
  }

  /**
   *
   */
  private List<Correspondence> relatedCorrespondence;

  /**
   *
   */
  public void print() {
    for (Correspondence item : this.relatedCorrespondence) {
      item.print();
    }
  }

  /**
   * @param correspondence
   */
  public void add(Correspondence correspondence) {
    this.relatedCorrespondence.add(correspondence);
  }

  /**
   * @param correspondence
   */
  public void remove(Correspondence correspondence) {
    this.relatedCorrespondence.remove(correspondence);
  }

  /**
   * @param correspondence
   */
  public void update(Correspondence correspondence) {
    int itemIndex = this.relatedCorrespondence.indexOf(correspondence);
    this.relatedCorrespondence.set(itemIndex, correspondence);
  }

}
